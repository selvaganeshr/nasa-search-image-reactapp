import React from 'react';
import axios from 'axios';
import SearchBar from './SearchBar';
import ImageList from './imageList';
import nasa from './nasa.png';

import { I18nProvider, LOCALES } from './i18n';
import translate from './i18n/translate'

class App extends React.Component  {
    state = { images: [], locale: LOCALES.ENGLISH };

    onSearchSubmit = async (term) => {
      const response = await
      axios.get('http://localhost:8080/getMarsPhotos/sol/10', {
        headers: {
            'Content-Type': 'application/json'
        }
      })
      this.setState({ images: response.data.photos })
      console.log(response);
    }

    render() {
      return (
        <I18nProvider locale={this.state.locale}>
        <div>
            <center><img src={nasa} alt="NASA API" width="75" /></center>
            <center> <h1 style={{color: 'red'}}> {translate("nasa - mars rover photos")} </h1> </center>
            <hr/>
            <label style={{color: 'blue'}}>Choose language: </label>
            <button onClick={() => this.setState({locale: LOCALES.ENGLISH})}> English </button> &nbsp;&nbsp;
            <button onClick={() => this.setState({locale: LOCALES.DUTCH})}> Dutch </button> &nbsp;&nbsp;
            <button onClick={() => this.setState({locale: LOCALES.FRENCH})}> French </button><br/><br/>
            <SearchBar userSubmit={this.onSearchSubmit}/>
            <br/><span>{translate("found")}: {this.state.images.length} {translate("images")}</span><br/><br/>
            <ImageList foundImages={this.state.images} />
        </div>
        </I18nProvider>
      )
   }
}

export default App;
