export const LOCALES = {
  ENGLISH: 'en',
  DUTCH: 'nl',
  FRENCH: 'fr',
};
