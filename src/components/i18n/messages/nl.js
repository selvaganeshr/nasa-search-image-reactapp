import { LOCALES } from '../locales';

export default {
  [LOCALES.DUTCH]: {
    'hello':'Hallo',
    'nasa - mars rover photos':'NASA - Mars Rover Fotos',
    'images':'afbeeldingen',
    'found':'Gevonden',
    'Rover Image Search':'Rover Afbeeldingen Zoeken',
  }
}
