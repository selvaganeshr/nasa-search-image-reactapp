import { LOCALES } from '../locales';

export default {
  [LOCALES.FRENCH]: {
    'hello':'Bonjour',
    'nasa - mars rover photos':'NASA - Photos de Mars Rover',
    'images':'images',
    'found':'a trouvé',
    'Rover Image Search':'Recherche d`images Rover',
  }
}
