import { LOCALES } from '../locales';

export default {
  [LOCALES.ENGLISH]: {
    'hello':'Hello',
    'nasa - mars rover photos':'NASA - Mars Rover Photos',
    'images':'images',
    'found':'Found',
    'Rover Image Search':'Rover Image Search',
  }
}
