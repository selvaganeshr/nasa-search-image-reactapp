import en from './en';
import nl from './nl';
import fr from './fr';

export default {
  ...en,
  ...nl,
  ...fr,
}
