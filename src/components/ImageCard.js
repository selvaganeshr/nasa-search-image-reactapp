import React, { Component } from 'react';

class ImageCard extends Component {

  constructor(props) {
        super(props);
        this.imageRef = React.createRef();
  }

  componentDidMount() {
        console.log(this.imageRef);
  }

  render() {
    return (
      <div>
          <img ref={this.imageRef}
               src={this.props.image.img_src}
               alt={this.props.image.id} />
      </div>
    )
  }
}

export default ImageCard;
