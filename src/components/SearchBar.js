import React from 'react';
import './SearchBar.css';
import translate from './i18n/translate'

class SearchBar extends React.Component {
    state = { val: '', camera: ''}

    onInputChange = (event) => {
      this.setState({ val: event.target.value })
    }

    onFormSubmit = (event) => {
      event.preventDefault();
      console.log(this.state.val);
      this.props.userSubmit(this.state.val);
    }

    render() {
        return (
            <div>
            <form onSubmit={this.onFormSubmit} className="flexContainer">
                <label><center><h2>{translate('Rover Image Search')}: </h2></center></label>
                &emsp;<label><h4> Parameters:</h4> </label>
                &emsp;<label><h4> Sol:</h4> </label>
                    <input className="inputStyle"
                      type="text"
                      size="5"
                      value={this.state.val}
                      onChange={this.onInputChange}
                    /> <br/> <br/>
                
            </form>
            </div>
        )
    }
}

export default SearchBar;
