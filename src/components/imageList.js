import React from 'react';
import ImageCard from './ImageCard';
import './imageList.css';

const ImageList = (props) => {
    const imgs = props.foundImages.map(img => {
          return <img key={img.id} src={img.img_src} alt={img.sol} />
        //return <ImageCard key={img.id} image={img} />
    });
    console.log(props.foundImages);

    return (
        <div className="img_list">{imgs}</div>
    )
}

export default ImageList;
